package com.example.starter;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;
import io.vertx.core.Vertx;
import io.vertx.core.json.Json;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.ext.mongo.MongoClient;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.handler.BodyHandler;

public class MainVerticle extends AbstractVerticle {

  private static final Logger logger = LoggerFactory.getLogger(MainVerticle.class);

  private MongoClient mongoClient;
  private String mongoCollection = "user";

  @Override
  public void start(Future<Void> startFuture) throws Exception {

    JsonObject mongoConfig = new JsonObject();
    mongoConfig.put("db_name", "default_db");
    mongoConfig.put("connection_string", "mongodb://localhost:27017");

    mongoClient = MongoClient.createShared(vertx, mongoConfig);

    Router router = Router.router(vertx);
    router.route().handler(BodyHandler.create());
    router.route().handler(context -> {
      logger.info(context.request().rawMethod() + "  " +context.request().absoluteURI());
      context.next();
    });
    router.get("/values").handler(this::list);
    router.post("/values").handler(this::create);
    router.get("/values/:id").handler(this::get);
    router.put("/values/:id").handler(this::update);
    router.delete("/values/:id").handler(this::delete);

    JwtExample jwtExample = new JwtExample(vertx);
    jwtExample.mount(router);

    vertx.createHttpServer()
      .requestHandler(router)
      .listen(8888, http -> {
      if (http.succeeded()) {
        startFuture.complete();
        logger.info("HTTP server started on port 8888");
      } else {
        startFuture.fail(http.cause());
      }
    });
  }

  private void list(RoutingContext context) {
    JsonObject query = new JsonObject();
    mongoClient.find(mongoCollection, query, result -> {
      if (result.succeeded()) {
        context.response()
          .setStatusCode(200)
          .end(Json.encode(result.result()));
      } else {
        context.fail(500, result.cause());
      }
    });
  }

  private void create(RoutingContext context) {
    JsonObject obj = context.getBodyAsJson();
    mongoClient.insert(mongoCollection, obj, result -> {
      if (result.succeeded()) {
        context.response()
          .setStatusCode(201)
          .end();
      } else {
        context.fail(500, result.cause());
      }
    });
  }

  private void get(RoutingContext context) {
    String id = context.pathParam("id");
    JsonObject query = new JsonObject();
    query.put("_id", id);
    mongoClient.findOne(mongoCollection, query, null, result -> {
      if (result.succeeded()) {
        context.response()
          .setStatusCode(200)
          .end(Json.encode(result.result()));
      } else {
        context.fail(500, result.cause());
      }
    });
  }

  private void update(RoutingContext context) {
    String id = context.pathParam("id");
    JsonObject query = new JsonObject();
    query.put("_id", id);
    JsonObject obj = context.getBodyAsJson();
    mongoClient.findOneAndUpdate(mongoCollection, query, obj, result -> {
      if (result.succeeded()) {
        context.response()
          .setStatusCode(202)
          .end(Json.encode(result.result()));
      } else {
        context.fail(500, result.cause());
      }
    });
  }

  private void delete(RoutingContext context) {
    String id = context.pathParam("id");
    JsonObject query = new JsonObject();
    query.put("_id", id);
    mongoClient.findOneAndDelete(mongoCollection, query, result -> {
      if (result.succeeded()) {
        context.response()
          .setStatusCode(204)
          .end(Json.encode(result.result()));
      } else {
        context.fail(500, result.cause());
      }
    });
  }

  public static void main(String[] args) {
    Vertx.vertx().deployVerticle(new MainVerticle());
  }
}
