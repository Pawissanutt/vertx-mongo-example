package com.example.starter;

import io.vertx.core.Vertx;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.auth.PubSecKeyOptions;
import io.vertx.ext.auth.jwt.JWTAuth;
import io.vertx.ext.auth.jwt.JWTAuthOptions;
import io.vertx.ext.jwt.JWTOptions;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.client.HttpRequest;
import io.vertx.ext.web.client.WebClient;
import io.vertx.ext.web.client.WebClientOptions;
import io.vertx.ext.web.handler.JWTAuthHandler;

import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;

public class JwtExample {

  private Vertx vertx;
  private JWTAuth jwt;

  public JwtExample(Vertx vertx) {
    this.vertx = vertx;
    JWTAuthOptions config = new JWTAuthOptions()
      .addPubSecKey(new PubSecKeyOptions()
        .setAlgorithm("HS256")
        .setPublicKey("place holder")
        .setSymmetric(true));
    jwt = JWTAuth.create(vertx, config);
  }



  // mount handler to router
  public void mount(Router router) {

    // protect the API
    router.route("/protected/*").handler(JWTAuthHandler.create(jwt, "/unprotected"));

    // this route is excluded from the auth handler
    router.post("/unprotected/token").handler(this::genToken);

    router.get("/protected/p1").handler(this::resourceRequireP1);
    router.get("/protected/p4").handler(this::resourceRequireP4);


  }

  //create token with permissions
  private void genToken(RoutingContext context) {
    JWTOptions jwtOptions = new JWTOptions().setAlgorithm("HS256");
    JsonObject claims = new JsonObject();

    claims.put("user", "user")
      .put("permissions", new JsonArray()
        .add("p1")
        .add("p2")
        .add("p3")
      );

    context.response()
      .setStatusCode(200)
      .putHeader("Content-Type", "application/jwt")
      .end(jwt.generateToken(claims, jwtOptions));
  }

  private void resourceRequireP1(RoutingContext context) {
    JsonObject claim = context.user().principal();
    List<String> permissions = claim.getJsonArray("permissions").getList();
    returnResponse(context, permissions.contains("p1"));
  }

  private void resourceRequireP4(RoutingContext context) {
    JsonObject claim = context.user().principal();
    List<String> permissions = claim.getJsonArray("permissions").getList();
    returnResponse(context, permissions.contains("p4"));
  }

  private void returnResponse(RoutingContext context, boolean pass) {
    if (pass) {
      context.response()
        .setStatusCode(200)
        .end("Pass");
    } else {
      context.response()
        .setStatusCode(403)
        .end("Forbidden");
    }
  }

  // try to access resources with Jwt
  public static void main(String[] args) {
    Vertx vertx = Vertx.vertx();
    WebClientOptions options = new WebClientOptions();
    options.setDefaultHost("localhost");
    options.setDefaultPort(8888);
    WebClient client = WebClient.create(vertx, options);
    var future = getToken(client);
    future.whenComplete((token,e) -> {
      if (e == null) {
        System.out.println("get token success");
        System.out.println("token : " + token);
        var access1 = access(client, "/protected/p1", token);
        var access2 = access(client, "/protected/p4", token);
        received(access1, "/protected/p1");
        received(access2, "/protected/p4");
      } else {
        System.err.println(e);
      }
    });
  }

  private static void received(CompletionStage<String> stage, String path) {
    stage.whenComplete((res,e) -> {
      if (e == null) {
        System.out.println("Send \""+path+"\" success");
        System.out.println("result : " + res);
      } else {
        System.out.println("Access \""+path+"\" failed");
      }
    });
  }

  private static CompletionStage<String> getToken(WebClient client) {
    CompletableFuture<String> future = new CompletableFuture<>();
    client.post("/unprotected/token")
      .send(res -> {
        if (res.succeeded()) {
          future.complete(res.result().bodyAsString());
        } else {
          future.completeExceptionally(res.cause());
        }
      });
    return future;
  }

  private static CompletionStage<String> access(WebClient client,
                                         String path,
                                         String token) {
    CompletableFuture<String> future = new CompletableFuture<>();
    HttpRequest<Buffer> request = client.get(path);
    request.headers()
      .add("Authorization", "Bearer " + token);
    request.send(res -> {
      if (res.succeeded()) {
        future.complete(res.result().bodyAsString());
      } else {
        future.completeExceptionally(res.cause());
      }
    });
    return future;
  }

}
