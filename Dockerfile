FROM openjdk:11.0.2-slim

ENV JAR_FILE starter-1.0.0-SNAPSHOT-fat.jar
ENV HOME /app

COPY target/$JAR_FILE $HOME/

WORKDIR $HOME
EXPOSE 8888
ENTRYPOINT ["sh", "-c"]
CMD ["exec java \
        -jar $JAR_FILE  "]
